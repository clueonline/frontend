import { Menu} from 'antd';
import React, {Component} from "react";
import { Link } from "react-router-dom";
import {keycloak} from "../../index";

export class MainMenu extends Component {

  logout() {
    console.log("User logged out");
    keycloak.logout()
  }

  render(){
    return(
      <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={['1']}
        style={{lineHeight: '64px'}}
      >
        {/*<Menu.Item key="0">*/}
        {/*  <Link to={'/testing'}>*/}
        {/*    Testing*/}
        {/*  </Link>*/}
        {/*</Menu.Item>*/}
        <Menu.Item key="1">
          <Link to={'/'}>
            Dashboard
          </Link>
        </Menu.Item>

        <Menu.Item key="2">
          <Link to={'/projects'}>
            Projects
          </Link>
        </Menu.Item>

        <Menu.Item key="3">
          <Link to={'/library'}>
            Library</Link>
        </Menu.Item>

        <Menu.Item key="4" onClick={this.logout}>
          Log out
        </Menu.Item>
      </Menu>
    )
  }
}

