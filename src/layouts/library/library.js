import React from "react";
import { Table, Tag, Input, Button, Icon,  } from "antd";
import Highlighter from 'react-highlight-words';
import request from "superagent";
import {getToken} from "../../utls/utls";
import {API_URL} from "../../config";

export class Library extends React.Component{
  state = {
    searchText: '',
    searchedColumn: '',
    data: []
  };

  getLibraryDetails() {
    request.get(`${API_URL}/library`)
      .set('Authorization', getToken())
      .query({resource: 'display'})
      .end((error, res) => {
        if (res) {
          let data = JSON.parse(res.text).data;
          this.setState({data});
          console.log(this.state)
        } else {
          console.error(error)
        }
      });
  }

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };



  columns = [
    {
      title: 'Display Name',
      dataIndex: 'name',
      key: 'name',
      ...this.getColumnSearchProps('name'),
    },
    {
      title: 'Alias',
      dataIndex: 'alias',
      key: 'alias',
      ...this.getColumnSearchProps('alias'),
      render: text =>
        text.map(value =>
          this.state.searchedColumn === 'alias' ? (
            <Tag color={'red'}>
              <Highlighter
                highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                searchWords={[this.state.searchText]}
                autoEscape
                textToHighlight={value.toString()}
              />
            </Tag>
          ) : (
            <Tag color={'cyan'}>{value}</Tag>
          )),

    },
    {
      title: 'Length\'s',
      dataIndex: 'lengths',
      key: 'lengths',
      ...this.getColumnSearchProps('lengths'),
      render: text =>
        text.map(value =>
        this.state.searchedColumn === 'lengths' ? (
          <Tag color={'red'}>
          <Highlighter
            highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
            searchWords={[this.state.searchText]}
            autoEscape
            textToHighlight={value.toString()}
          />
          </Tag>
        ) : (
          <Tag color={'orange'}>{value}</Tag>
        )),

    }
  ];

  componentDidMount() {
    this.getLibraryDetails()
  }

  render() {
    return (
    <Table columns={this.columns} dataSource={this.state.data} onChange={this.onChange} />
    )
  }

}