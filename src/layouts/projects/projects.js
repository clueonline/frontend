import React, {Component} from "react";
import { Switch, Route, Link } from 'react-router-dom';
import {Divider, Table} from "antd";
import {AddProject} from "./add_Project";
import request from "superagent";
import {getToken, formatProjectData} from "../../utls/utls";
import {Create} from "../bom/create/create";
import {OverView} from "./details/overView";
import {API_URL} from "../../config";

export class Projects extends Component {

  state = {
    visible: false,
    projects: []
  };

  project_columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Project',
      dataIndex: 'project',
      key: 'project',
    },
    {
      title: 'Client',
      dataIndex: 'client',
      key: 'client',
    },
    {
      title: 'BOM\'s',
      dataIndex: 'boms',
      key: 'boms',
    },
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => (
        <span>
          <Link to={`/projects/${record.key}`}>View</Link>
          {/*<a href={`/projects/${record.key}`}>View</a>*/}
          <Divider type="vertical" />
          <Link to={`/projects/${record.key}/create`}>Add BOM</Link>
          <Divider type="vertical" />
          <a>Archive</a>
        </span>
      ),
    },
  ];

  getProjects(){
    request.get(`${API_URL}/projects`)
      .set('Authorization', getToken())
      .end((error, res) => {
        if (res) {
          let result = [];
          let data = JSON.parse(res.text);
          if (res.status === 200){
            data.forEach(project => result.push(formatProjectData(project)));
            this.setState({projects: result})
          }
        } else {
          console.error(error)
        }
      } )
  }

  addProject(data){
    let project = formatProjectData(data);
    let projects = this.state.projects;
    projects.push(project);
    projects.sort((a, b) => a.id > b.id);
    this.setState({projects: projects})
  }

  componentDidMount() {
    this.getProjects();
  }


  render(){
    let match = this.props.match;
    return(
      <Switch>

        <Route path={`${match.path}/:projectId/create`} component={Create} />
        <Route path={`${match.path}/:projectId`} component={OverView} />

        <Route path={match.path}>
          <div>
            <AddProject addProject={this.addProject.bind(this)}/>
            <Table columns={this.project_columns} dataSource={this.state.projects} />
          </div>
        </Route>
      </Switch>

    )
  }
}

