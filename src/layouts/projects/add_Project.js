import {Button, Modal, Form, Input, Icon} from 'antd';
import React from "react";
import request from "superagent";
import {getToken} from "../../utls/utls";
import {API_URL} from "../../config";


const ProjectCreateForm = Form.create({ name: 'form_in_modal' })(
  // eslint-disable-next-line
  class extends React.Component {
    render() {
      const { visible, onCancel, onCreate, form } = this.props;
      const { getFieldDecorator } = form;
      return (
        <Modal
          visible={visible}
          title="Create a new collection"
          okText="Create"
          onCancel={onCancel}
          onOk={onCreate}
        >
          <Form layout="vertical">

            <Form.Item label="Project Id">
              {getFieldDecorator('id', {
                rules: [{ required: true, message: 'Project Id is required' }],
              })(<Input />)}
            </Form.Item>

            <Form.Item label="Project NAme">
              {getFieldDecorator('project', {
                rules: [{ required: true, message: 'Project Name is required' }],
              })(<Input />)}
            </Form.Item>

            <Form.Item label="Client Name">
              {getFieldDecorator('client', {
                rules: [{ required: true, message: 'Project Client name is required' }],
              })(<Input />)}
            </Form.Item>

          </Form>
        </Modal>
      );
    }
  },
);

export class AddProject extends React.Component {
  state = {
    visible: false,
  };

  showModal = () => {
    this.setState({ visible: true });
  };

  handleCancel = () => {
    const { form } = this.formRef.props;
    this.setState({ visible: false });
    form.resetFields()

  };

  handleCreate = () => {
    const { form } = this.formRef.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }

      console.log('Received values of form: ', values);
      this.postProject(values);
      form.resetFields();
      this.setState({ visible: false });
    });
  };

  saveFormRef = formRef => {
    this.formRef = formRef;
  };

  postProject(values){
    request.post(`${API_URL}/projects`)
      .send(values)
      .set('Authorization', getToken())
      .end((error, res) => {
        if (res) {
          let data = JSON.parse(res.text);
          console.log(data);
          this.props.addProject(data.data)
        } else {
          console.error(error)
        }
      } )
  }

  render() {
    return (
      <div>

        <Button type={'primary'} onClick={this.showModal}>
          Add Project
          <Icon type="plus-circle" />
        </Button>

        <ProjectCreateForm
          wrappedComponentRef={this.saveFormRef}
          visible={this.state.visible}
          onCancel={this.handleCancel}
          onCreate={this.handleCreate}
        />
      </div>
    );
  }
}
