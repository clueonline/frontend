import React from "react";
import {ProjectHeader} from "./projectHeader";
import {ProjectStats} from "./projectStats";
import {ProjectBoms} from "./projectBoms";
import request from "superagent";
import {getToken} from "../../../utls/utls";
import {API_URL} from "../../../config";

export class OverView extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      header: '',
      stats: '',
      boms: ''
    }
  };

  getHeaderDetails() {
    request.get(`${API_URL}/project/${this.props.match.params.projectId}`)
      .set('Authorization', getToken())
      .query({resource: 'header'})
      .end((error, res) => {
        if (res) {
          let header = JSON.parse(res.text).header;
          this.setState({header});
        } else {
          console.error(error)
        }
      });
  }

  getStatsDetails() {
    request.get(`${API_URL}/project/${this.props.match.params.projectId}`)
      .set('Authorization', getToken())
      .query({resource: 'stats'})
      .end((error, res) => {
        if (res) {
          let stats = JSON.parse(res.text).stats;
          this.setState({stats});
        } else {
          console.error(error)
        }
      });
  }

  getBomsDetails() {
    request.get(`${API_URL}/project/${this.props.match.params.projectId}`)
      .set('Authorization', getToken())
      .query({resource: 'boms'})
      .end((error, res) => {
        if (res) {
          let boms = JSON.parse(res.text).boms;
          this.setState({boms});
        } else {
          console.error(error)
        }
      });
  }

  componentDidMount() {
    this.getHeaderDetails();
    this.getStatsDetails();
    this.getBomsDetails();
  }

  render() {
    return (
      <div>
        {console.log('overView render return', this.state)}
        <ProjectHeader data={this.state.header}/>
        <ProjectStats data={this.state.stats}/>
        <ProjectBoms data={this.state.boms}/>
      </div>
    )
  }
}