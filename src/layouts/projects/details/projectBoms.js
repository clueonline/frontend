import React from "react";
import { Link } from "react-router-dom";
import { Table, Tag } from "antd";

export class ProjectBoms extends React.Component{
  render() {

    const columns = [
      {
        title: 'Created',
        dataIndex: 'created',
        key: 'created',
      },
      {
        title: 'description',
        dataIndex: 'description',
        key: 'description',
      },
      {
        title: 'Tags',
        dataIndex: 'tags',
        key: 'tags',
        render: (text) => ( text.map((value) => <Tag color="volcano">{value}</Tag>))
      },
      {
        title: 'Action',
        dataIndex: 'action',
        key: 'action',
        render: (text, record) => (<Link to={`/bom/${record.key}`}>View</Link>)
      }
    ];
    return(
    <Table dataSource={this.props.data} columns={columns} />
    )
  }
}