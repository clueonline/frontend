import React from "react"
import { Row, Col, Statistic } from "antd";

export class ProjectStats extends React.Component{
  render() {
    return (
      <Row gutter={16}>
        {stat('Created', this.props.data.created)}
        {stat('Last Active', this.props.data.last_active)}
        {stat('No. of BOM\'s', this.props.data.bom_count)}
        {stat('Last BOM Created', this.props.data.bom_created)}
      </Row>
    )
  }
}

const stat = (title, value) => (
  <Col span={12}>
    <Statistic title={title} value={value} />
  </Col>
);