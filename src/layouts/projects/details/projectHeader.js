import React from "react";
import {PageHeader, Button, Descriptions, Affix, Typography} from "antd";

const { Paragraph } = Typography;

export class ProjectHeader extends React.Component{
  render() {
    return (
      <Affix offsetTop={80} >
        <PageHeader
          ghost={false}
          onBack={() => window.history.back()}
          title={`${this.props.data.name} (${this.props.data.project_id})`}
          extra={[
            // TODO set up the function for the buttons
            <Button key="2" type={'primary'}>Create BOM</Button>,
            <Button key="1" type="danger">
              Archive
            </Button>,
          ]}
        >
          <Descriptions size="small" column={3}>
            <Descriptions.Item label="Project Name"><Paragraph copyable>{this.props.data.name}</Paragraph></Descriptions.Item>
            <Descriptions.Item label="Project ID"><Paragraph copyable>{this.props.data.project_id}</Paragraph></Descriptions.Item>
            <Descriptions.Item label="Client"><Paragraph copyable>{this.props.data.client}</Paragraph></Descriptions.Item>
          </Descriptions>
        </PageHeader>
      </Affix>
    )
  }
}