import React, {Component} from "react";
import {LastProjects} from "./lastProjects";
import {LastBoms} from "./lastBoms";
import request from "superagent";
import {formatProjectData, getToken} from "../../utls/utls";
import {API_URL} from "../../config";

export class Dashboard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      projects: [],
      boms: []
    }
  };

  getProjectDetails() {
    request.get(`${API_URL}/projects`)
      .set('Authorization', getToken())
      .query({count: 10})
      .end((error, res) => {
        let projects = [];
        if (res && res.status === 200){
          let data = JSON.parse(res.text);
          data.forEach(project => projects.push(formatProjectData(project)));
          this.setState({projects});
          console.log(this.state)
        } else {
          console.error(error)
        }
      });
  }

  getBomsDetails() {
    request.get(`${API_URL}/bom`)
      .set('Authorization', getToken())
      .query({count: 10})
      .end((error, res) => {
        if (res) {
          let boms = JSON.parse(res.text).boms;
          this.setState({boms});
          console.log(this.state)
        } else {
          console.error(error)
        }
      });
  }

  componentDidMount() {
    this.getBomsDetails();
    this.getProjectDetails();
  }

  render(){
    return(
      <div>
        <div>
          <LastProjects data={this.state.projects}/>
        </div>
        <br />
        <div>
          <LastBoms data={this.state.boms}/>
        </div>
      </div>
    )
  }
}

