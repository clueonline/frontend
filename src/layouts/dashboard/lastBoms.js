import React from "react";
import {Divider, Table, Tag} from "antd";
import {Link} from "react-router-dom";

export class LastBoms extends React.Component {

  columns = [
    {
      title: 'Created',
      dataIndex: 'created',
      key: 'created',
    },
    {
      title: 'description',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'Tags',
      dataIndex: 'tags',
      key: 'tags',
      render: (text) => ( text.map((value) => <Tag color="volcano">{value}</Tag>))
    },
    {
      title: 'Action',
      dataIndex: 'action',
      key: 'action',
      render: (text, record) => (<Link to={`/bom/${record.key}`}>View</Link>)
    }
  ];

  bom_data = [
    {
      key: '1',
      name: 'John Brown',
      age: 32,
      address: 'New York No. 1 Lake Park',
      tags: ['nice', 'developer'],
    },
    {
      key: '2',
      name: 'Jim Green',
      age: 42,
      address: 'London No. 1 Lake Park',
      tags: ['loser'],
    },
    {
      key: '3',
      name: 'Joe Black',
      age: 32,
      address: 'Sidney No. 1 Lake Park',
      tags: ['cool', 'teacher'],
    },
  ];

  render() {
    return (
      <div>
        <h1>Latest BOM Results</h1>

        <Table columns={this.columns} dataSource={this.props.data} pagination={false}/>
      </div>
  )
  }
}