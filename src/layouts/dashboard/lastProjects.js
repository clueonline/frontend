import React from "react";
import {Table} from "antd";

export class LastProjects extends React.Component {

  columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Project',
      dataIndex: 'project',
      key: 'project',
    },
    {
      title: 'Client',
      dataIndex: 'client',
      key: 'client',
    },
    {
      title: 'BOM\'s',
      dataIndex: 'boms',
      key: 'boms',
    }
  ];

  render() {
    return (
      <div>
        <h1>Latest Active Projects</h1>
        <Table columns={this.columns} dataSource={this.props.data} pagination={false}/>
      </div>
    )
  }
}