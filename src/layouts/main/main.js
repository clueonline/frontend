import { Layout, Breadcrumb } from 'antd';
import React, {Component} from "react";
import { Switch, Route } from 'react-router-dom'
import request from "superagent";
import {getToken, printTokens} from "../../utls/utls";
import {MainMenu} from "../menus/main_menu";
import {Dashboard} from "../dashboard/dashboard";
import {Projects} from "../projects/projects";
import {Result} from "../bom/results/result";
import {Library} from "../library/library";
import {API_URL} from "../../config";

const { Header, Content, Footer } = Layout;

export class Main extends Component {

  state = {
    message: 'End of the World'
  };

  getWelcome(){
    request.get(`${API_URL}`)
      .set('Authorization', getToken())
      .end((error, res) => {
        if (res) {
          let data = JSON.parse(res.text);
          this.setState({message: data.message})
        } else {
          console.error(error)
        }
      } )
  }

  componentWillMount() {
    this.getWelcome();
    printTokens();
  }

  render(){
    return(
      <Layout className="layout">
        <Header style={{position: "sticky", top: "0", zIndex: 1}}>
          <div className="logo" />
          <MainMenu/>
        </Header>
        <Content style={{padding: '0 50px'}}>
          <div style={{background: '#fff', padding: 24, minHeight: 280}}>

            <Switch>
              <Route path={'/library'} component={Library} />
              <Route path={'/bom/:result_id'} component={Result} />
              <Route path={'/projects'} component={Projects} />
              <Route path={'/'} component={Dashboard} />
            </Switch>

          </div>
        </Content>
        <Footer style={{textAlign: 'center', position: "sticky", bottom: "0"}}>Jim Fitzpatrick ©2020</Footer>
      </Layout>
    )

  }
}

