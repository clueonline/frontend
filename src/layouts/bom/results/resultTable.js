import React from "react";
import {Table, Col} from "antd";

export class ResultTable extends React.Component{

  columns = [
    {
      title: '',
      dataIndex: 'material',
      key: 'material',
      align: 'center',
      render: (text, record) => (
        record.status === 'missing'?
          <b style={{fontSize: 'large', color: 'red'}}>{text} - Missing Parts</b>
          : <b>{text}</b>
        ),
    },
    {
      title: 'Average',
      dataIndex: 'average',
      key: 'average',
      align: 'center',
      render: (text) => (text+'%')
    },
    {
      title: 'High',
      dataIndex: 'high',
      key: 'high',
      align: 'center',
      render: (text) => (text+'%')
    },
    {
      title: 'Low',
      dataIndex: 'low',
      key: 'low',
      align: 'center',
      render: (text) => (text+'%')
    }
  ];



  render() {
    return (
      <Col md={24} xl={16} xxl={12}>
      <Table
        dataSource={this.props.data.dataSource}
        columns={this.columns.concat(this.props.data.columns)}
        pagination={false}
        title={() => <b style={{fontSize: 'large'}}>Result Table</b>}
        bordered
      />
      </Col>
    )
  }

}