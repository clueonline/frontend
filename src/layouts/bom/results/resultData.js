import React from "react";
import {Divider} from "antd";
import {ResultDataMaterialRow} from "./resultDataMaterialRow";

export class ResultData extends React.Component{
  render() {
    return (
      <div>
        <Divider orientation={'left'}>
          <h1>Used Sections</h1>
        </Divider>
        {console.log('props', this.props.materials)}
        { this.props.materials.map(material => (<ResultDataMaterialRow key={material.key} material={material}/>))}
      </div>
    )
  }

}