import React from "react";
import {Row} from "antd";
import {ResultDataCard} from "./resultDataCard";

export class ResultDataMaterialRow extends React.Component{
  render() {
    return (
      <div>
      <Row>
        <h2>{this.props.material.name}</h2>
        <Row gutter={[8, 8]}>
          {
            this.props.material.beams.map(beam => <ResultDataCard key={beam.key} beam={beam}/>)
          }
        </Row>
      </Row>
        <br/>
      </div>
    )
  }
}