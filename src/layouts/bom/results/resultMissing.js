import React from "react";
import {Col, Table} from "antd";

export class ResultMissing extends React.Component{
  columns = [
    {
      title: 'Material',
      dataIndex: 'material',
      key: 'material',
      align: 'center'
    },
    {
      title: 'Item',
      dataIndex: 'item',
      key: 'item',
      align: 'center'
    },
    {
      title: 'Length',
      dataIndex: 'length',
      key: 'length',
      align: 'center'
    },
    {
      title: 'Qty',
      dataIndex: 'qty',
      key: 'qty',
      align: 'center'
    }
  ];

  dataSource = [
    {
      key: '1',
      material: 'UC 356x406x287',
      item: '2.4.3',
      length: 2000,
      qty: 2
    },
    {
      key: '2',
      material: 'UC 356x406x287',
      item: '2.4.4',
      length: 2000,
      qty: 2
    }
  ];

  render() {
    return (
      <Col md={16} xl={8}>
        <Table
          columns={this.columns}
          dataSource={this.props.data}
          pagination={false}
          title={() => <b style={{fontSize: 'large'}}>Missing Materials</b>}
          bordered
        />
      </Col>
    )
  }

}