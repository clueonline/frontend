import React from "react";
import { Row, Col, Descriptions} from 'antd';

export class ResultHeader extends React.Component{
  onClick = () => (console.log("button Pressed"));

  render() {
    return (
      <Row>
        <Row gutter={[16,16]}>

          <Col md={24} lg={18} xl={12} xxl={8}>
            <Descriptions title="Project Information">
              <Descriptions.Item label="Project Id">{this.props.project.project_id}</Descriptions.Item>
              <Descriptions.Item label="Project Name">{this.props.project.name}</Descriptions.Item>
              <Descriptions.Item label="Client Name">{this.props.project.client}</Descriptions.Item>
              <Descriptions.Item label="Total BOM's">{this.props.project.boms}</Descriptions.Item>
            </Descriptions>
          </Col>
          <Col md={24} lg={18} xl={12} xxl={8}>
            <Descriptions title="Current BOM">
              <Descriptions.Item label="Date">{this.props.bom.date}</Descriptions.Item>

              <Descriptions.Item label="File Used">{this.props.bom.filename}</Descriptions.Item>
              <Descriptions.Item label="Description">{this.props.bom.description}</Descriptions.Item>

            </Descriptions>
          </Col>
        </Row>
      </Row>
    )
  }

}