import React from "react";
import {Col, Divider, Statistic} from "antd";

export class ResultDataItemsPart extends React.Component {
  render() {
    return (
      <div>
        <Col>
          <Col span={12}>
            <Statistic title="Items" value={this.props.part.item_no}/>
          </Col>
          <Col span={12}>
            <Statistic title="Length" value={this.props.part.length} valueStyle={{color: 'green'}}/>
          </Col>
          <Col span={12}>
            <Statistic title="QTY" value={this.props.part.qty} valueStyle={{color: 'red'}}/>
          </Col>
        </Col>
        <Divider/>
      </div>
    )
  }
}