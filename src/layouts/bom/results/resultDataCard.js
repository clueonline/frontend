import React from "react";
import {Card, Col, Progress, Row, Statistic} from "antd";
import {ResultDataItems} from "./resultDataItems";


export class ResultDataCard extends React.Component{
  render() {
    return (
      <Col md={8} lg={6} xxl={4}>
        <Card title={<Progress percent={this.props.beam.percent} />} >
          <Row>
            <Col span={12}>
              <Statistic title="Beam Length" value={this.props.beam.length} />
            </Col>
            <Col span={12}>
              <Statistic title="Waste" value={this.props.beam.waste} />
            </Col>
            <Col span={12}>
              <Statistic title="Usage Length" value={this.props.beam.usage} valueStyle={{color: 'green', fontSize: 'xx-large'}}/>
            </Col>
            <Col span={12}>
              <Statistic title="QTY" value={this.props.beam.qty} valueStyle={{color: 'red', fontSize: 'xx-large'}}/>
            </Col>
          </Row>

          <ResultDataItems parts={this.props.beam.parts} />

        </Card>
      </Col>
    )
  }
}