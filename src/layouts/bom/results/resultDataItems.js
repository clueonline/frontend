import React from "react";
import {Collapse, Row} from "antd";
import {ResultDataItemsPart} from "./resultDataItemsPart";

const { Panel } = Collapse;

export class ResultDataItems extends React.Component {
  render() {
    return (
      <Collapse>
        <Panel header="Items Consumed Per Beam" key="1">
          <Row gutter={[8, 8]}>
            { this.props.parts.map(part => <ResultDataItemsPart key={part.key} part={part}/>)}
          </Row>
        </Panel>
      </Collapse>
    )
  }
}