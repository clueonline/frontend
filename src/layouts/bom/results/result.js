import React from "react";
import {ResultHeader} from "./resultHeader";
import {ResultData} from "./resultData";
import {ResultTable} from "./resultTable";
import {ResultMissing} from "./resultMissing";
import {Row, PageHeader, Button, Affix} from "antd";
import request from "superagent";
import {getToken} from "../../../utls/utls";
import {API_URL} from "../../../config";

export class Result extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      result: {
        project: {
          name: '',
          client: '',
          project_id: '',
          boms: ''
        },
        bom: {
          header: {
            filename: '',
            data: '',
            description: ''
          },
          beams: [],
          result_table: {
            dataSource: [],
            columns: []
          },
          missing_table: []
        }
      }
      }
    };

  async getResultData() {
    return await request.get(`${API_URL}/result`)
      .set('Authorization', getToken())
      .query({result: this.props.match.params.result_id})
      .end((error, res) => {
        if(res) {
          let result = JSON.parse(res.text).result;
          this.setState({result});
          console.log('Loaded state', this.state.result.bom)
        } else {
          console.error(error)
        }
      })

  }

  async updateProcessingTime() {
    let process_finish = new Date().getTime();
    return await request.patch(`${API_URL}/result`)
      .send({
        result_uuid: this.props.match.params.result_id,
        data: {process_finish: process_finish},
        request: 'update_processing_time'
      })
      .set('Authorization', getToken())
      .end((error, res) => {
        if (res) {
          let data = JSON.parse(res.text);
          console.log(data);
        } else {
          console.error(error)
        }
      });
  }

  componentDidMount() {
    this.updateProcessingTime();
    this.getResultData();
  }

  render() {
    return (
      <div>
        <Affix offsetTop={80} >
        <PageHeader
          ghost={false}
          onBack={() => window.history.back()}
          backIcon={null}
          title="BOM Results"
          subTitle={`For project ${this.state.result.project.name}`}
          extra={[
            <Button type="primary" key={'1'}>
              View Project
            </Button>,
            <Button type="primary" key={'2'} >
              Rerun BOM
            </Button>
          ]}
        />
        </Affix>
        <ResultHeader project={this.state.result.project} bom={this.state.result.bom.header}/>
        <br/>
        <Row gutter={[16,16]}>
        <ResultTable data={this.state.result.bom.result_table}/>
          {
            this.state.result.bom.missing_table.length > 0 ?
            <ResultMissing data={this.state.result.bom.missing_table}/>
            : ''
          }
        </Row>
        <br/>
        <ResultData materials={this.state.result.bom.beams}/>
      </div>
    )
  }

}