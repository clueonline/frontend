import React from "react";
import {InfoPanel} from "./infoPanel";

export class StepThree extends React.Component {
  constructor(props) {
    super(props);
    this.desc = this.props.desc
  }
  render() {
    return (
      <div>
        <InfoPanel desc={this.desc} />
        <hr />
        <br/>
      </div>
    )
  }
}