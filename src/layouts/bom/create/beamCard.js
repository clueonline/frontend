import React from "react";
import {Card, Icon, Switch, Col, Tag, InputNumber, Checkbox, Popover} from "antd";

export class BeamCard extends React.Component{

  render_option(){
    if(this.props.status === 'missing'){
      return (
        <Card title={this.props.title} extra={<Tag color="red">Missing from Library</Tag>}>
          <h3>Add Lengths</h3>
          {this.props.lengths.map(item => (
            <NewLengthSwitch key={item.id} id={item.id} title={item.name} selected={item.selected} onChange={item.onChange} save={item.save}/>
          ))}
          <InputNumber
            min={0}
            precision={0}
            onPressEnter={(event) => this.props.onPressEnter(event, this.props.id)}
          />
        </Card>
      )
    } else {
      return (
        <Card title={this.props.title}>
          <h3>Choose Lengths</h3>
          {this.props.lengths.map(item => (
            <LengthSwitch key={item.id} id={item.id} title={item.name} selected={item.selected} onChange={item.onChange}/>
          ))}
        </Card>
      )
    }
  }

  render() {
    return(
      <Col span={8}>
        {this.render_option()}
      </Col>
    )
  }

}

class LengthSwitch extends React.Component{
  render() {
    return (
      <p>
        <Switch
          id={this.props.id}
          checkedChildren={<Icon type="check" />}
          unCheckedChildren={<Icon type="close" />}
          defaultChecked={this.props.selected}
          onChange={(checked, event) => this.props.onChange(checked, event, this.props.id)}
        />
        &nbsp;&nbsp;{this.props.title}&nbsp;&nbsp;
      </p>
    )
  }
}

class NewLengthSwitch extends React.Component{

  pop_over_content = (
    <div>
      <p>Save length to Library when finished.</p>
      <p>If not saved length will be used in this BOM only</p>
    </div>
  );

  render() {
    return (
      <p>
        <Switch
          id={this.props.id}
          checkedChildren={<Icon type="check" />}
          unCheckedChildren={<Icon type="close" />}
          // defaultChecked={true}
          defaultChecked={this.props.selected}
          onChange={(checked, event) => this.props.onChange(checked, event, this.props.id)}
        />
        &nbsp;&nbsp;{this.props.title}&nbsp;&nbsp;
        <Popover content={this.pop_over_content} title={'Save to Library'} placement="right">
          <Checkbox checked={this.props.save}>
            Save to Library
          </Checkbox>
        </Popover>
      </p>
    )
  }
}