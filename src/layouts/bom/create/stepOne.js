import React from "react";
import {Input} from "antd";
import {FileUpload} from "./fileUpload";
import {InfoPanel} from "./infoPanel";

const { TextArea } = Input;

export class StepOne extends React.Component {

  render() {
    return (
      <div>
        <InfoPanel desc={this.props.desc} />
        <hr />
        <br />
        <FileUpload handleComplete={this.props.handleComplete.bind(this)} disabled={this.props.disabled} project={this.props.desc.uuid}/>
        <br />
        <div>
          <label htmlFor={"description"}>Description</label>
          <TextArea id={"Description"}
                    autoSize={{ minRows: 2, maxRows: 6 }}
                    onChange={this.props.handleChange}
                    defaultValue={this.props.desc.description}
          />
        </div>
        <br />
      </div>
    )
  }
}