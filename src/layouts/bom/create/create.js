import React from "react";
import { Redirect } from 'react-router-dom'
import { Steps, Button, message, Row, Col } from 'antd';
import {StepThree} from "./stepThree";
import {StepOne} from "./stepOne";
import {StepTwo} from "./stepTwo";
import request from "superagent";
import {getToken} from "../../../utls/utls";
import {API_URL} from "../../../config";

const { Step } = Steps;

export class Create extends React.Component {
  constructor(props) {
    super(props);
    let date = new Date();
    this.state = {
      redirect: false,
      current: 0,
      disabled: true,
      desc: {
        uuid: '',
        id: '',
        project: '',
        client: '',
        date: `${date.toDateString()} ${date.toTimeString()}`,
        description: '',
        filename: '',
        file_id: '',
        beams: [],
        missing_beams: [],
        process_start: date.getTime(),
        process_finish: null
      }
    };
  }

  steps = [
    {
      title: 'Upload',
      description: "Upload a valid csv file",
      content: () => <StepOne
        desc={this.state.desc}
        handleChange={this.handleDescriptionChange.bind(this)}
        handleComplete={this.handleFileUploadComplete.bind(this)}
        disabled={this.state.disabled}
      />,
    },
    {
      title: 'Configure',
      description: "Configure the different materials",
      content: () => <StepTwo
        desc={this.state.desc}
        getBeamDetails={this.getBeamDetails.bind(this)}
        onPressEnter={this.onPressEnter.bind(this)}
      />,
    },
    {
      title: 'Review',
      description: "Review the BOM configure",
      content: () => <StepThree desc={this.state.desc}/>,
    },
  ];

  handleDescriptionChange(evt) {
    const {value} = evt.target;
    let desc = {...this.state.desc};
    desc.description = value;
    this.setState({desc})
  }

  handleFileUploadComplete(info) {
    if (info.file.status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === 'done') {
      message.success(`${info.file.name} file uploaded successfully`);
      let desc = {...this.state.desc};
      desc.filename = info.file.name;
      desc.file_id = info.file.response;
      this.setState({desc, disabled: false});
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  }

  onChangeToggle(checked, evt, id){
    let desc = this.state.desc;
    let main_id = id.split('-')[0];
    desc.beams.forEach(beam =>
      beam.id === main_id ? beam.lengths.forEach(length =>
        length.id === id ? length.selected = checked : '') : '');
    this.setState({desc});
  }

  onPressEnter(event, parent_uuid){
    let item = {
      id: parent_uuid + '-' + new Date().getTime(),
      name: event.target.value,
      selected: true,
      save: true,
      onChange: this.onChangeToggleLibrary.bind(this)
    };
    let desc = this.state.desc;
    desc.missing_beams.forEach(beam =>
      beam.id === parent_uuid ? beam.lengths.push(item) : ''
    );
    this.setState({desc});
  }

  onChangeToggleLibrary(checked, evt, id){
    let desc = this.state.desc;
    let main_id = id.split('-')[0];
    desc.missing_beams.forEach(beam =>
      beam.id === main_id ? beam.lengths.forEach(length =>
        length.id === id ? length.selected = checked : '') : '');
    this.setState({desc});
  }

  next() {
    const current = this.state.current + 1;
    this.setState({ current });
  }

  prev() {
    const current = this.state.current - 1;
    this.setState({ current });
  }

  componentDidMount() {
    this.getProjectDetails()
  }

  getProjectDetails(){
    request.get(`${API_URL}/projects`)
      .set('Authorization', getToken())
      .query({'project': this.props.match.params.projectId})
      .end((error, res) => {
        if (res) {
          let data = JSON.parse(res.text);
          let desc = this.state.desc;
          desc.project = data.name;
          desc.client = data.client;
          desc.id = data.project_id;
          desc.uuid = data.uuid;
          this.setState({desc});
        } else {
          console.error(error)
        }
      } )
  }

  getBeamDetails(){
    if (this.state.desc.beams.length === 0){
      request.get(`${API_URL}/library`)
        .set('Authorization', getToken())
        .query({'project': this.props.match.params.projectId})
        .query({'file_uuid': this.state.desc.file_id})
        .end((error, res) => {
          if (res) {
            let data = JSON.parse(res.text);
            let desc = this.state.desc;
            desc.beams = data;
            desc.beams.forEach(item =>
            {
              item.lengths.forEach(length => length.onChange = this.onChangeToggle.bind(this));
              if(item.status === 'missing'){
                desc.missing_beams.push(item)
              }
            }
              );

            this.setState({desc});
            console.log(this.state)
          } else {
            console.error(error)
          }
        } )
    }
  }

  async done() {
    message.success('Processing complete!');
    // send materials to be added to the library
    if (this.state.desc.missing_beams.length > 0) {
      this.upload_library_data();
    }
    // send details to create a bom result page
    // let result = "hhhsflkjhg";
    this.upload_setup_data();
    // send user to the result page
    // this.setState({redirect: true, result:result});

  }

  async upload_library_data(){
    return await request.post(`${API_URL}/library`)
      .send({data: this.state.desc.missing_beams})
      .set('Authorization', getToken())
      .end((error, res) => {
        if (res) {
          let data = JSON.parse(res.text);
          console.log(data);
          message.success('Materials added to library');
        } else {
          console.error(error)
        }
      });
  }

  async upload_setup_data(){
    let desc = this.state.desc;
    desc.process_finish = new Date().getTime();
    this.setState({desc});
    return await request.post(`${API_URL}/result`)
      .send({data: this.state.desc})
      .set('Authorization', getToken())
      .end((error, res) => {
        if (res) {
          let data = JSON.parse(res.text);
          console.log(data);
          let result = data.result_id;
          this.setState({redirect: true, result:result});

        } else {
          console.error(error)
        }
      });
  }

render() {
    const { current } = this.state;
    if (this.state.redirect) {
      return <Redirect push to={`/bom/${this.state.result}`} />;
    }
    return (
      <Row>

        <Col span={6}>
        <Steps current={current} direction="vertical">
          {this.steps.map(item => (
            <Step key={item.title} title={item.title} description={item.description} />
          ))}
        </Steps>
        </Col>
        <Col span={18}>
        <div className="steps-content">{this.steps[current].content()}</div>

        <div className="steps-action">
          {current < this.steps.length - 1 && (
            <Button type="primary" onClick={() => this.next()} disabled={this.state.disabled}>
              Next
            </Button>
          )}
          {current === this.steps.length - 1 && (
            <Button type="primary" onClick={() => this.done()}>
              Done
            </Button>
          )}
          {current > 0 && (
            <Button style={{ marginLeft: 8 }} onClick={() => this.prev()}>
              Previous
            </Button>
          )}
        </div>
        </Col>
      </Row>
    );
  }
}
