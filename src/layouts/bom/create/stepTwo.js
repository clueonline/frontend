import React from "react";
import {Row} from "antd";
import {InfoPanel} from "./infoPanel";
import {BeamCard} from "./beamCard";

export class StepTwo extends React.Component {
  componentDidMount() {
    this.props.getBeamDetails()
  }

  display_beams = () => (this.props.desc.beams.length > 0 ?
      <Row>
        <h1>Section Sizes</h1>
        <Row gutter={[8, 8]}>
            {this.props.desc.beams.map(item => ( item.status !== 'missing' ?
                <BeamCard key={item.id} id={item.id} title={item.name} lengths={item.lengths}/> :
                ''
            ))}
        </Row>
      </Row> :
      ''
  );

  display_missing_beams = () => (this.props.desc.missing_beams.length > 0 ?
    <Row>
      <h2>Missing From Library</h2>
      <Row gutter={[8, 8]}>
        {this.props.desc.missing_beams.map(item =>
          <BeamCard
            key={item.id}
            id={item.id}
            title={item.name}
            lengths={item.lengths}
            status={item.status}
            onPressEnter={this.props.onPressEnter}/>
          )}
      </Row>
    </Row> :
      ''
  );

  render() {
    return (
      <div>
        <InfoPanel desc={this.props.desc} />
        <hr />
        <br />
        {this.display_beams()}
        {this.display_missing_beams()}
        <br/>
      </div>

    )
  }
}
