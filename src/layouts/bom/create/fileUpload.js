import React from "react";
import { Upload, Button, Icon } from 'antd';
import {getToken} from "../../../utls/utls";
import {API_URL} from "../../../config";

export class  FileUpload extends React.Component{

  render() {
    let values = {
      name: 'file',
      action: `${API_URL}/upload`,
      headers: {
        authorization: getToken(),
      },
      data: {project_id: this.props.project}
    };
    return (
      // <Upload {...values} onChange={this.props.handleComplete} disabled={!this.props.disabled}>
      <Upload {...values} onChange={this.props.handleComplete}>
        <Button>
          <Icon type="upload" /> Click to Upload
        </Button>
      </Upload>
    )
  }
}