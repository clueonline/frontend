import React from "react";
import {Descriptions} from "antd";

export class InfoPanel extends React.Component{

  add_to_library = () => ( this.props.desc.missing_beams.length > 0 ?
      <Descriptions.Item label="Adding to Library">
      {this.props.desc.missing_beams.map(item => (
          <NewMaterialLength key={item.id} title={item.name} lengths={item.lengths}/>
        )
      )}
      </Descriptions.Item>
      : ''
  );

  render() {


    return (
      <Descriptions title="BOM Information">
        <Descriptions.Item label="Project Id">{this.props.desc.id}</Descriptions.Item>
        <Descriptions.Item label="Project Name">{this.props.desc.project}</Descriptions.Item>
        <Descriptions.Item label="Client Name">{this.props.desc.client}</Descriptions.Item>
        <Descriptions.Item label="Date">{this.props.desc.date}</Descriptions.Item>
        <Descriptions.Item label="File Upload">{this.props.desc.filename}</Descriptions.Item>
        <Descriptions.Item label="Description">{this.props.desc.description}</Descriptions.Item>
        <Descriptions.Item label="Materials">
          {this.props.desc.beams.map(item => (
            <MaterialLength key={item.id} title={item.name} lengths={item.lengths}/>
            )
          )}
        </Descriptions.Item>
        {this.add_to_library()}
      </Descriptions>
    )
  }
}

class MaterialLength extends React.Component{

  render() {
    return (
      <span><b>{this.props.title}</b> ({this.props.lengths.map(item => item.selected ? `${item.name}, ` : ``)});&nbsp;</span>
    )
  }
}

class NewMaterialLength extends React.Component{

  render() {
    return (
      <span><b>{this.props.title}</b> ({this.props.lengths.map(item => item.save ? `${item.name}, ` : ``)});&nbsp;</span>
    )
  }
}

