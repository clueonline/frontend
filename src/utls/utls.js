export function printTokens(){
  let token = localStorage.getItem("react-token");
  let refreshToken = localStorage.getItem("react-refresh-token");

  console.log(token);
  console.log(refreshToken)
}

export function getToken() {
  return localStorage.getItem("react-token")
}

export function formatProjectData(data){
  return  {
    key: data.uuid,
    id: data.project_id,
    project: data.name,
    client: data.client,
    boms: data.boms
  }
}