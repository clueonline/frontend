export const API_URL = 'https://api.jfitzpatrick.me';
export const AUTH_URL = 'http://auth.jfitzpatrick.me';

// export const API_URL = 'http://localhost:5000';
// export const API_URL = 'http://172.21.0.5:5000';
// export const AUTH_URL = 'http://172.21.0.4:8080';
// export const AUTH_URL = 'http://0.0.0.0:8080';
