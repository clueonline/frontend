FROM node:alpine

EXPOSE 3000
RUN mkdir /app
WORKDIR /app

COPY . /app

RUN npm install
RUN npm run build
RUN yarn global add serve

CMD serve -p 3000 -s /app/build
